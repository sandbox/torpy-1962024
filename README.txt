Menu Query Trail Fix

Drupal, by default does not recognise URL queries when loading menu items. Thus when there are multiple menu items with the same path but different URL queries, Drupal loads first menu item that has the matching path without looking at the query.

This is even true in cases where there are multiple menu items, some with queries, some without and you are browsing a page without a query.

This module provides a workaround for that by overriding the menu item and active menu trail for any page that has a menu item and has query parameter set.

No configuration is required, just install and you're off.

CAVEATS:
- May cause a performance hit, only use if you really need this.
    * Naturally, since each matching menu item has to be loaded and its query inspected, there is a performance hit on each request.
    * Database and static caches are being leveraged however the performance hit cannot be completely solved.

- If there are multiple menu items with the same base path and query parameters, only the first item is used.

- If you land on a page with a query which does not have a menu item but the base path exists in the database, the module will try and use the menu item without a query. E.g.:
  * If you land on a views page with a filter set in the URL such as "my/views/page?filter=2" which has a menu item, it will be set correctly.
  * If you then select another filter and apply to land on URL which looks like "my/views/page?filter=2&anotherfilter=3", the module will try and find a menu item which just matches "my/views/page" (without the query) and set that as the active item.

TODO:
- Provide alter callbacks.
